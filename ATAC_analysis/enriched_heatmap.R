## Minimal example to produce EnrichHeatmap
## handles BigWig and massive matrix, I would ideally Sbatch it until the final plotting step
library(EnrichedHeatmap)
library(data.table)
library(rtracklayer)
library(circlize)
library(GenomicRanges)
setwd("/data/pilot_output") ### change if run through Sbatch
#load the narrowpeak file and transform into genomicRanges element
df = fread("bwa/merged_replicate/macs2/narrow_peak/A.mRp.clN_peaks.narrowPeak")
targets = makeGRangesFromDataFrame(df, seqnames.field = "V1", start.field = "V2", end.field = "V3")
## just to see what on Earth that thing is
head(targets)
# Extending the peak 5kb in each direction; I think 1kb is enough through, but for the sake of tutorial
ExtendSize <- 1000
targets.extended  <- resize(targets, fix = "center", width = ExtendSize*2)
## loading the bigwig, according to tutorial this is the efficient way
BigWig = rtracklayer::import("bwa//merged_replicate/bigwig/A.mRp.clN.bigWig", format= "BigWig", selection = BigWigSelection(targets.extended))
## making heatmap matrix. width=1 as it wants to take in the peak centre and then extend it by predifined window size 
# it is a *very* large matrix and it *will* crash RStudio. DO NOT INVOKE
normMatrix <- normalizeToMatrix(signal = BigWig, 
                                target = resize(targets, fix = "center", width = 1), 
                                background = 0, 
                                keep = c(0, 0.99),      #/ minimal value to the 99th percentile
                                target_ratio = 0,
                                mean_mode = "w0",       #/ see ?EnrichedHeatmap on other options
                                value_column = "score", #/ = the name of the 4th column of the bigwig
                                extend = ExtendSize)
# gradient function, percentile to avoid the otliers skew the map
col_fun = circlize::colorRamp2(quantile(normMatrix, c(0, .99)), c("darkblue", "red"))
#/ heatmap function:
EH <- EnrichedHeatmap( mat = normMatrix, 
                       pos_line = TRUE, #/ no dashed lines around the start
                       border = FALSE,   #/ no box around heatmap
                       col = col_fun,    #/ color gradients from above
                       column_title = "ATAC_A", #/ column title 
                       column_title_gp = gpar(fontsize = 15, fontfamily = "sans"),
                       use_raster = TRUE, raster_quality = 10, raster_device = "png",
                       #/ turn off background colors
                       rect_gp = gpar(col = "transparent"), 
                       #/ legend options:
                       heatmap_legend_param = list(
                         legend_direction = "horizontal",
                         title = "normalized counts"),
                       #/ options for the profile plot on top of the heatmap:
                       top_annotation = HeatmapAnnotation(
                         enriched = anno_enriched(
                           gp = gpar(col = "black", lty = 1, lwd=2),
                           col="black")
                       )
) # end of EH function
saveRDS(EH, "EH_heatmap_AD.RDS")
# If run through Sbatch, stop here and in RSTudio afterwards
EH = readRDS("EH_heatmap_AD.RDS")
draw(EH,                                 #/ plot the heatmap from above 
     heatmap_legend_side = "bottom",     #/ we want the legend below the heatmap
     annotation_legend_side = "bottom",  #/ legend on the bottom side
     padding = unit(c(4, 4, 4, 4), "mm") #/ some padding to avoid labels beyond plot borders
)