# Codebase multi-omics Steinmetz group



## WHAT IS THIS?

In this git you should find scripts to download, preprocess and do *basic* analysis of multi-sample bulk ATAC- and RNA-seq data. 


## EXPERIMENTAL DESIGN

We feed our animals ad libitum and then stop simultaneously. We sample at the designated time-points post-feeding, and then refeed at 5 and 20 days in order to investigate:

1) General expression dynamics related to feeding and starvation
2) Contrasting expression based on refeeding at different quiescence depths
3) Regulatory landscape associated with starvation and refeeding

![Experimental_design](https://i.imgur.com/uy31zpz.png)


## CONTENTS OF THIS GIT
This is a very high-level summary of what's in the different directories. Hopefully there should be a markdown document explaining individual decisions and code snippets in more detail.

ATAC-seq;
Follows the nf-core procedures. Should download data from repository and then run the analyses more or less autonomously; up to and including peak calling and summary statistics (i.e. PCA and suchlike) and footprinting analysis.

RNA-seq;  
Follows the nf-core procedure, with STAR, Salmon and DEseq2. Similar to ATAC; should download and process from reads through summary statistics etc.

DEseq analyses;
DEG calling; significant differences between short- and long-term starvation in RNA- and ATAC-seq, in isolation. 

Integrated analyses;
DEGs with various , coregulation. Follows EPIC-DREM, more or less.

Author:
Øystein Monsen