# for some backend issues you may need to change the nextflow.config. The safest* way of doing this is to alter
#e.g.  ~/.nextflow/assets/nf-core/rnaseq/nextflow.config, since it will tend to refer to other paths and things

#ln -s /cluster/projects/nn11045k/refgenome/GCF_932526225.1_jaNemVect1.1_genomic.gtf . #assumes that there is a catalogue called 'refgenome' with the relevant files. can be ignored if you already have the links or files in directory
#ln -s /cluster/projects/nn11045k/refgenome/GCF_932526225.1_jaNemVect1.1_genomic.fna . 

nextflow run \
    nf-core/rnaseq  \ #do not alter
    -r 3.14.0 \ #may change as the pipeline is updated. Monitor this
    --input samplesheet.csv  \ #a sample sheet following the structure of the one in this git
    --outdir nvec_samp \ #where nextflow will make its catalog structure
    --gtf GCF_932526225.1_jaNemVect1.1_genomic.gtf \ #we generated softlinks above
    --fasta GCF_932526225.1_jaNemVect1.1_genomic.fna \
    --igenomes_ignore \ #do not alter
    --genome null \ #do not alter
    -profile singularity #do not alter
