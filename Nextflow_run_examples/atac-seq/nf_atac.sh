#This follows a slightly different format than RNA-seq, but it's trivial - the only difference is that we supply parameters via a yaml instead of via this command line script.
#Both are included, it's a matter of pure preference.


#ln -s /cluster/projects/nn11045k/refgenome/GCF_932526225.1_jaNemVect1.1_genomic.gtf . #assumes that there is a catalogue called 'refgenome' with the relevant files. can be ignored if you already have the links or files in directory
#ln -s /cluster/projects/nn11045k/refgenome/GCF_932526225.1_jaNemVect1.1_genomic.fna . 

nextflow run nf-core/atacseq --input samplesheet.csv -profile singularity -params-file params.yaml --outdir atac_out