##THIS FILE ASSUMES THAT YOU HAVE A VALID KEY PAIR TO THE SERVER WHERE THE READ FILES LIVE AND THAT THE FILE STRUCTURE IS SOLVED THIS ALMOST CERTAINLY DOESN'T FULLY APPLY, SO YOU NEED TO ADJUST CERTAIN FILES HERE.

#execute using sh download_data.sh keyfile path_string destination corresponding to the variables below

#https://docs.nrec.no/ssh.html is a good general tutorial for ssh stuff, though it notably takes an nrec-based perspective.

SSH_PUBLICKEY=$1 #this means that the first argument provided while running this downloader is the path to the ssh public key corresponding to a private key on the server. This is necessary in lieu of a password setup.
PROJECTPATH=$2 #this refers to the path within the server where the catalogue in question lives. It is at the present set as the second argument provided via the command line
DESTINATION=$3 # provide a path for the destination, i.e. where you want the files to be downloaded

#note that the path variables here can be set either locally or globally. Global paths may get very long and clunky, but this is up to the user.

scp -i $SSH_PUBLICKEY username@server:$PROJECTPATH/ $DESTINATION #this uses scp instead of rsync. Maybe change?
